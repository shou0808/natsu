<?php get_header(); ?>
    <main>
      <section class="blog">
        <div class="container">
          <h1><?php the_title(); ?></h1>
          <div class="day"><?php the_time('Y年m月d日'); ?></div>
          <article>
               <?php while(have_posts()): the_post(); the_content(); endwhile; ?>
          </article>
        </div>
      </section>
    </main>
<?php get_footer(); ?>