<?php get_header(); ?>
    <div class="main-visual">
        <?php if ( get_header_image() ) : ?>
	        <img src="<?php header_image(); ?>" alt="">
        <?php else: ?>
            <?php  echo '<img id="frontpage-image" src="'.get_template_directory_uri().'/images/main.png"/>'; ?>
        <?php endif; ?>
    </div>
        <!----- main ----->
        <main id="main">
           <section class="main-section">
           
           </section>
        </main>
        <!----- /main ----->
<?php get_footer(); ?>