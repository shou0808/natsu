<?php
  // JS・CSSファイルを読み込む
  function add_files() {
    // WordPress提供のjquery.jsを読み込まない
    wp_deregister_script('jquery');
    // jQueryの読み込み
    wp_enqueue_script( 'jquery', get_template_directory_uri() .'/vender/jquery-3.6.0.min.js', "", "20210623", false );
    // サイト共通JS
    wp_enqueue_script( 'main', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '20210623', true );
    // サイト共通のCSSの読み込み
    wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css', "", '20210623' );
    // サイト共通のCSSの読み込み
    wp_enqueue_style( 'main', get_template_directory_uri() . '/css/nomarize.css', "", '20210623' );
  }
  add_action('wp_enqueue_scripts', 'add_files');

  // ナビゲーションメニュー
  function register_my_menu() {
    register_nav_menu( 'main-menu','Main Menu');
  }
  add_action( 'after_setup_theme', 'register_my_menu' );

  // メインビジュアル設定
  add_theme_support( 'custom-header' );

  // アイキャッチ画像を有効にする。
  add_theme_support('post-thumbnails');

// bodyにクラスを付ける
  add_filter( 'body_class', 'add_page_slug_class_name' );
  function add_page_slug_class_name( $classes ) {
    if ( is_page() ) {
      $page = get_post( get_the_ID() );
      $classes[] = $page->post_name;

      $parent_id = $page->post_parent;
      if ( $parent_id ) {
        $classes[] = get_post($parent_id)->post_name . '-child';
      }
    }
    return $classes;
  }

  // 追加のショートコード
  function url_shortcode() {
    $url = get_template_directory_uri();
    return $url;
  }
  add_shortcode( 'urlcode', 'url_shortcode' );
