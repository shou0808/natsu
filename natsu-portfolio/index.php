<?php get_header(); ?>
    <main>
      <!-- 投稿ページ -->
      <section class="">
        <div class="container">
          <h1><?php the_title(); ?></h1>
          <div class=""><?php the_time('Y年m月d日'); ?></div>
          <div class="">
              <?php if(has_post_thumbnail()):?>
                <?php the_post_thumbnail('thumbnail'); ?>
              <?php else:?>
                <img src="<?php echo get_template_directory_uri();?>/images/contents.png">
              <?php endif; ?>
          </div>
          <article>
               <?php while(have_posts()): the_post(); the_content(); endwhile; ?>
          </article>
        </div>
      </section>
    </main>
<?php get_footer(); ?>