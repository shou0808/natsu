<?php get_header(); ?>
    <main>
      <section class="">
        <div class="container">
          <article>
              <?php while(have_posts()): the_post(); the_content(); endwhile; ?>
          </article>
        </div>
      </section>
    </main>
<?php get_footer(); ?>



