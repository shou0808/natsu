<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title><?php the_title(); ?></title>
    <meta name="description" content="natsu portfolio">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?php wp_head(); ?>
</head>

<body <?php body_class();?>>
        <!----- header----->
        <header id="top" class="wrap">
            <div class="flex">
                <div class="logo">
                    <h2>
                        <a href="<?php echo home_url();?>">natsu portfolio</a>
                    </h2>                
                </div>
                <div class="nav">
                        <?php
                            wp_nav_menu( array(
                                'theme_location' => 'main-menu'
                            ) );
                        ?>
                </div>
            </div>
        </header>
        <!----- /header ----->